import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import Layout from './components/Layout';
import Tiles from './components/Tiles';
import Activity from './components/Activity';
import { request } from './utils/request';
import { getPosts, getComments } from './redux/actionCreator';
import Modal from './components/Modal';

function App() {
  const dispatch = useDispatch();
  const dataSelector = useSelector(state => state);

  useEffect( () => {
    dispatch(getPosts());
    dispatch(getComments());
  }, []);

  console.log(useSelector(state => state))

  const dataOne = {
    data: dataSelector?.posts || [],
    detail: {
      title: 'Videos',
      viewAllTitle: 'Browse all videos',
      uploadTitle: 'Upload your own videos',
      icon: 'fa-upload'
    },
    withUpload: true,
    column: 3,
    bigTiles: true,
    responsive: true
  };

  const activityData = {
    data: dataSelector?.comments || [],
    detail: {
      title: 'Activity',
      viewAllTitle: 'View timeline/Filter activities',
    }
  }

  const dataPeople = {
    data: [
      {text: 'George'},
      {text: 'Alexandra'},
      {text: 'James'},
      {text: 'Patrick'},
      {text: 'Ammy'},
    ],
    detail: {
      title: 'People',
      viewAllTitle: 'View All',
      uploadTitle: 'Show your work',
      icon: 'fa-upload'
    },
    withUpload: true,
    column: 3,
    bigTiles: true,
    responsive: true
  }

  const dataDocument = {
    data: [
      {text: 'Mobile ui & ux guide'},
      {text: 'HTML course'},
      {text: 'Marketing trends 2022'},
      {text: 'CSS tips and trick'},
      {text: 'Javascript Bootcamp'},
    ],
    detail: {
      title: 'Document',
      viewAllTitle: 'Browse all document',
      uploadTitle: 'Share your document',
      icon: 'fa-upload'
    },
    withUpload: true,
    column: 3,
    bigTiles: true,
    responsive: true
  }

  const dataChannel = {
    data: [
      {text: 'Google'},
      {text: 'Yahoo'},
      {text: 'Microsoft'},
      {text: 'Behance'},
      {text: 'Dribbble'},
      {text: 'Canva'},
      {text: 'Figma'},
      {text: 'Adobe'},
    ],
    detail: {
      title: 'Channel',
      viewAllTitle: 'Browse all channel',
    },
    column: 2,
    headerBorder: true
  }

  return (
    <Layout>
      <div className="row">
        <div className="col-lg-8">
          <Tiles {...dataOne}/>
          <Tiles {...dataPeople}/>
           <Tiles {...dataDocument}/>
        </div>
        <div className="col-lg-4">
          <Activity {...activityData}/>
          <Tiles {...dataChannel}/>
        </div>
      </div>
      <Modal />
    </Layout>
  );
}

export default App;
