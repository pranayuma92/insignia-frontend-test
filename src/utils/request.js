import { BASE_API, APP_ID } from './constants';

export const request = async (
	method, endpoint = null, data = null, token = null, rest = null
) => {
	const options = {
        method,
        body: data,
        headers : {
            'content-type': 'application/json',
            'accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'app-id': APP_ID
        },
        ...rest,
    };

    if(token){
        Object.assign(options.headers, { 
            'Authorization': 'Bearer ' + token 
        });
    }

    const url = BASE_API + endpoint;

    const res = await fetch(url, options);
    
    if (!res.ok) {
        return { status: res.status, error: { message: res.statusText } };
    }

    const resData = await res.json();
    return { status: res.status, data: resData };
};
