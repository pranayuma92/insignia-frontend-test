import React from 'react';
import { TilesWrapper, TilesHeader, HeaderBorder } from './styles';

const Tiles = ({ column, data = [], detail, withUpload, bigTiles, headerBorder }) => {

	return (
		<div className="mt-5 mb-5">
			<TilesHeader>
				<h4>{detail?.title}</h4>
				<p>{detail?.viewAllTitle}</p>
			</TilesHeader>
			{ headerBorder && <HeaderBorder /> }
			<TilesWrapper column={column}>
				{
					data?.map((item, index) => {
						const classStyle = bigTiles && (parseInt(index) === 0) ? 'tiles double' : 'tiles';
						return (
							<div key={parseInt(index)} className={classStyle}>
								{item?.image && <img src={item?.image} />}
								<div className="tile-desc">
									<h4>{item?.owner?.firstName} {item?.owner?.lastName}</h4>
									{item?.text}
								</div>
							</div>
						)
					})
				}
				{ withUpload && (
					<div className="tiles opaque"><i  className={`fa ${detail?.icon}`} /><p> {detail?.uploadTitle}</p></div>
				)}
			</TilesWrapper>
		</div>
	);
};

export default Tiles;
