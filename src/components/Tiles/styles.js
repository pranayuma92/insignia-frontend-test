import styled from 'styled-components';
import { defaultColor } from '../../utils/defaultColor';

export const TilesWrapper = styled.div`
	display: grid;
    grid-gap: 5px;
    grid-template-columns: repeat(${(props) => (props.column)}, minmax(120px, 1fr));
    grid-auto-rows: minmax(120px, 180px);
    list-style: none;

    .tiles-row {
	  	grid-template-columns: none;
	}

	.tiles {
		position: relative;
	  	margin: 0 10px 10px 0;
	  	background: ${defaultColor.SECONDARY};
	  	cursor: pointer;

	  	h4 {
	  		display: none;
	  	}
	}

	.tiles img {
	    width: 100%;
	    height: 100%;
	    object-fit: cover;
	}

	.double {
	  	grid-column-end: span 2;
	  	grid-row-end: span 2;

	  	h4 {
	  		display: block;
	  	}
	}

	.opaque {
		background: transparent;
		border: 3px solid ${defaultColor.SECONDARY};
		margin: 0 10px 10px 0;
		display: flex;
	    justify-content: center;
	    align-items: center;
	    padding: 30px;
	    gap: 15px;
	    cursor: pointer;

	    p {
	    	font-size: 18px;
	    	font-weight: 600;
	    	color: ${defaultColor.SECONDARY};
	    	margin: 0;
	    }

	    i {
	    	color: ${defaultColor.SECONDARY};
	    	border: 2px solid ${defaultColor.SECONDARY};
		    padding: 10px;
		    border-radius: 50%;
	    }
	}

	.tile-desc {
	    position: absolute;
	    bottom: 0;
	    padding: 10px;
	    width: 100%;
		background: ${defaultColor.SECONDARY};
	    color: ${defaultColor.PRIMARY};
	}

	@media (max-width: 768px) {
	    display: flex;
    	width: 100%;
    	overflow: auto;

	    .double, .tiles {
		   	height: 150px;
		    margin: 0 10px 10px 0;
		    display: flex;
		    flex: 0 0 250px;
		    box-sizing: border-box;
	    }
	}
`;

export const TilesHeader = styled.div`
	display: flex;
	justify-content: space-between;
	color: ${defaultColor.SECONDARY};
	margin-right: 10px;
`;

export const HeaderBorder = styled.hr`
	margin: 1rem 0;
    color: inherit;
    border: 0;
    border-top: 3px solid ${defaultColor.SECONDARY};
    opacity: 1;
`;
