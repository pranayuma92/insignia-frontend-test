import React from 'react';
import { FooterWrapper } from './styles';

const Footer = () => {
	return (
		<FooterWrapper className="container">
			<div className="social mt-4 mb-5">
				<i className="fa fa-twitter" />
				<i className="fa fa-linkedin" />
				<i className="fa fa-facebook" />
			</div>
			<div className="navbar-nav mb-4">
		      	<a className="nav-item nav-link" href="#">About</a>
		      	<a className="nav-item nav-link" href="#">For Business</a>
		      	<a className="nav-item nav-link" href="#">Suggestion</a>
		      	<a className="nav-item nav-link" href="#">Help & FAQs</a>
		      	<a className="nav-item nav-link" href="#">Contacts</a>
		      	<a className="nav-item nav-link" href="#">Pricing</a>
	    	</div>
	    	<div className="copyright">
	    		<p>&copy; Copyright 2022 company, inc</p>
	    	</div>
	    	<div className="navbar-nav pb-5">
		      	<a className="nav-item nav-link" href="#">Privacy</a>
		      	<a className="nav-item nav-link" href="#">Terms</a>
	    	</div>
		</FooterWrapper>
	);
};

export default Footer;
