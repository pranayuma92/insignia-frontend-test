import styled from 'styled-components';
import { defaultColor } from '../../utils/defaultColor';

export const FooterWrapper = styled.footer`
	border-top: 4px solid ${defaultColor.SECONDARY};
	color: ${defaultColor.SECONDARY};

	p {
		margin: 0;
	}

	.social i {
	    padding: 15px 20px;
	    background: ${defaultColor.SECONDARY};
	    text-align: center;
	    vertical-align: middle;
	    color: ${defaultColor.PRIMARY};
	    font-size: 20px;
	    margin-right: 12px;
	    cursor: pointer;
	}

	.navbar-nav {
		display: flex;
	    flex-flow: row wrap;
	    gap: 20px;
	}

	.nav-item {
		color: ${defaultColor.SECONDARY};
		font-size: 16px;
		display: inline-block;
	}

	.nav-item:not(:last-child):after {
		content:'/';
		margin-left: 18px;
	}

	@media (max-width: 768px) {
		display: flex;
	    flex-flow: column;
	    justify-content: center;
	    align-items: center;

	    .navbar-nav {
			justify-content: center;
		}

	    .nav-item {
			font-size: 14px;
		}
	}
`;
