import React from 'react';
import { Wrapper } from './styles';

const NavMenu = () => {
	return (
		<Wrapper className="navbar navbar-expand-lg">
			<div className="container">
			  	<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
			    	<span className="navbar-toggler-icon"></span>
			  	</button>
			  	<div className="collapse navbar-collapse" id="navbarNavAltMarkup">
			    	<div className="navbar-nav">
				      	<a className="nav-item nav-link" href="#">Videos</a>
				      	<a className="nav-item nav-link" href="#">People</a>
				      	<a className="nav-item nav-link" href="#">Documents</a>
				      	<a className="nav-item nav-link" href="#">Events</a>
				      	<a className="nav-item nav-link" href="#">Communities</a>
				      	<a className="nav-item nav-link" href="#">Favorites</a>
				      	<a className="nav-item nav-link" href="#">Channels</a>
			    	</div>
			  	</div>
			</div>
		</Wrapper>
	);
};

export default NavMenu;
