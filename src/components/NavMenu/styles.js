import styled from 'styled-components';
import { defaultColor } from '../../utils/defaultColor';

export const Wrapper = styled.nav`
	color: ${defaultColor.SECONDARY};

	.container {
		border-top: 2px solid ${defaultColor.SECONDARY};
		border-bottom: 2px solid ${defaultColor.SECONDARY};
	}

	.nav-item {
		color: ${defaultColor.SECONDARY};
		font-size: 20px;
	}

	.nav-item:not(:last-child):after {
		content:'/';
		margin-left: 18px;
	}

	@media(max-width: 1023px) {
		display: none;
	}
`;