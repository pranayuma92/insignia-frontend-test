import React from 'react';
import { Wrapper } from './styles';
import Header from '../Header';
import Footer from '../Footer';

const Layout = ({ children }) => {
	return (
		<Wrapper>
			<Header />
			<div className="container">
				{ children }
			</div>
			<Footer />
		</Wrapper>
	);
};

export default Layout;
