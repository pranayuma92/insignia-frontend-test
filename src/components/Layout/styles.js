import styled from 'styled-components';
import { defaultColor } from '../../utils/defaultColor';

export const Wrapper = styled.section`
	background: ${defaultColor.PRIMARY};
	padding: 0;
	margin: 0;
	overflow-x: hidden;

	@media (max-width: 768px){
		h1, h2, h3, h4 {
			font-size: 18px;
		}

		p {
			font-size: 14px;
		}
	}
`;
