import React from 'react';
import ActivityItem from './ActivityItem';
import { ActivityHeader, ItemContainer } from './styles';

const Activity = ({ data, detail }) => {

	return (
		<div className="mt-5 mb-5">
			<ActivityHeader>
				<h4>{detail?.title}</h4>
				<p>{detail?.viewAllTitle}</p>
			</ActivityHeader>
			<ItemContainer>
				{
					data?.map((item, index) => (
						<ActivityItem item={item} key={index}/>
					))
				}
			</ItemContainer>
		</div>
	);
};

export default Activity;
