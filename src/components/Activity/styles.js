import styled from 'styled-components';
import { defaultColor } from '../../utils/defaultColor';

export const ActivityHeader = styled.div`
	display: flex;
	justify-content: space-between;
	color: ${defaultColor.SECONDARY};
	margin-right: 10px;
	border-bottom: 3px solid ${defaultColor.SECONDARY};
`;

export const ItemContainer = styled.div`
	border-bottom: 2px solid ${defaultColor.SECONDARY};
	cursor: pointer;
`;

export const ItemWrapper = styled.div`
	display: flex;
	color: ${defaultColor.SECONDARY};
	position: relative;

	.detail-side {
		line-height: 20px;
		margin-left: 10px;

		* {
			margin: 0;
			padding: 0;
		}

		h4 {
			font-size: 20px;
		}

		span {
			font-size: 16px;
			font-weight: 400;
		}
	}

	:hover:after {
		content: '';
	    width: calc(100% + 10px);
	    border: 3px solid ${defaultColor.SECONDARY};
	    position: absolute;
	    height: calc(100% + 15px);
	    left: -10px;
	    top: -5px;
	    margin: 0;
	}

	:hover {
		.delete {
			display: block;
		}
	}

	.delete {
		width: 15px;
	    height: 15px;
	    border-top: 35px solid ${defaultColor.SECONDARY};
	    border-left: 35px solid transparent;
	    position: absolute;
	    right: 0;
	    top: -5px;
	    display: none;
	}

	.delete:before {
	    content: 'x';
	    position: absolute;
	    top: -38px;
	    color: #953e46;
	    left: -14px;
	}
`;
