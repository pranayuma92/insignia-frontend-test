import React from 'react';
import { ItemWrapper } from './styles';

const ActivityItem = ({ item }) => {
	return (
		<ItemWrapper className="mt-3 mb-3">
			<div className="img-side"><img src={item?.owner?.picture} /></div>
			<div className="detail-side">
				<h4>{item?.owner?.firstName} {item?.owner?.lastName} <span>Commented</span></h4>
				<p>{item?.message}</p>
				<p><i className="fa fa-comment-dots" /> 5 minutes ago</p>
			</div>
			<div className="delete"></div>
		</ItemWrapper>
	);
};

export default ActivityItem;
