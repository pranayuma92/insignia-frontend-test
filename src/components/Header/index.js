import React, { useState } from 'react';
import NavMenu from '../NavMenu';
import { Navbar, Title, MobileWrapper, MobileNav, MobileSearch, ModalHeader } from './styles';
import Modal from '../Modal';

const Header = () => {
	const [isOpen, setOpen] = useState(false);

	const handleModalClose = () => {
		setOpen(!isOpen);
	}

	return (
		<>
			<Navbar className="navbar navbar-expand-lg justify-content-between">
				<div className="container">
				  	<Title className="navbar-brand">Social<span>Network</span></Title>
					<div className="navbar-nav">
					  	<form className="form-inline">
						    <div className="input-group">
						      	<input type="text" className="form-control" placeholder="Find..." aria-label="Find..." aria-describedby="basic-addon1" />
						      	<div className="input-group-prepend">
						        	<span className="input-group-text" id="basic-addon1"><i className="fa fa-search" /></span>
						      	</div>
						    </div>
						</form>
						<button type="button" className="btn btn-danger"><i className="fa fa-arrow-up" /> Upload</button>
						<div className="card">
		     	 			<div className="row ">
		        				<div className="col-md-4">
		            				<i className="fa fa-person" />
		          				</div>
					          	<div className="col-md-8">
						            <div className="card-block px-2 py-2">
						              <h4 className="card-title">Waseem</h4>
						              <p>Arshad</p>
						            </div>
					          	</div>
			    			</div>
			    		</div>
					</div>
				</div>
			</Navbar>
			<MobileWrapper>
				<MobileNav>
					<i className="fa fa-align-justify" onClick={() => setOpen(!isOpen)} />
					<Title className="navbar-brand">Social<span>Network</span></Title>
					<div>
						<i className="fa fa-upload" />
						<i className="fa fa-person" />
					</div>
				</MobileNav>
				<MobileSearch>
					<form className="form-inline">
					    <div className="input-group">
					      	<input type="text" className="form-control" placeholder="Find..." aria-label="Find..." aria-describedby="basic-addon1" />
					      	<div className="input-group-prepend">
					        	<span className="input-group-text" id="basic-addon1"><i className="fa fa-search" /></span>
					      	</div>
					    </div>
					</form>
				</MobileSearch>
			</MobileWrapper>
			<NavMenu />
			<ModalHeader>
				<Modal isOpen={isOpen} close={handleModalClose}>
					<div className="main-menu">
						<h2>Main Menu</h2>
						<div className="list-menu">
							<ul>
								<li>Videos</li>
						      	<li>People</li>
						      	<li>Documents</li>
						      	<li>Events</li>
						      	<li>Communities</li>
						      	<li>Favorites</li>
						      	<li>Channels</li>
							</ul>
						</div>
					</div>
				</Modal>
			</ModalHeader>
		</>
	);
};

export default Header;
