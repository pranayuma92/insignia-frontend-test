import styled from 'styled-components';
import { defaultColor } from '../../utils/defaultColor';

export const Navbar = styled.nav`

	.input-group {
		width: 450px;
	}

	input,
	.input-group-text {
		border: none;
		border-radius: 0;
		background: ${defaultColor.SECONDARY};
		color: ${defaultColor.PRIMARY};
	}

	.btn-danger {
		border: none;
		border-radius: 0;
		background: ${defaultColor.SECONDARY};
		color: ${defaultColor.PRIMARY};
		height: 50px;
		width: 100px;
		font-weight: 700;
	}

	.navbar-nav {
		display: flex;
    	align-items: center;
    	gap: 30px;

	}

	.input-group-text {
		height: 50px;
	}

	.card {
		border-radius: 0;
		border: none;
		height: 50px;
		background: transparent;
		color: ${defaultColor.SECONDARY};
		margin-right: -20px;
    	width: 130px;
	}

	.card-title {
		font-size: 16px;
		margin: 0;

	}

	p {
		font-size: 14px;
		margin: 0;
	}

	.fa-person {
		padding: 15px 20px;
	    background: ${defaultColor.SECONDARY};
	    text-align: center;
	    vertical-align: middle;
	    color: ${defaultColor.PRIMARY};
	    font-size: 20px;
	}

	@media(max-width: 1023px) {
		display: none;
	}
`;

export const Title = styled.h1`
	color: ${defaultColor.SECONDARY};
	font-size: 32px;
	font-weight: 600;

	span {
		font-weight: 300;
	}
`;

export const MobileWrapper = styled.div`
	@media(min-width: 1024px) {
		display: none;
	}
`;

export const MobileNav = styled.div`
	padding: 20px;
    display: flex;
    justify-content: space-between;

    i {
    	color: ${defaultColor.SECONDARY};
    	margin-right: 20px;
    	font-size: 18px;
    }

    i:last-child {
    	margin-right: 0;
    }
`;

export const MobileSearch = styled.div`
	padding: 0 20px;

	input,
	.input-group-text {
		border: none;
		border-radius: 0;
		background: ${defaultColor.SECONDARY};
		color: ${defaultColor.PRIMARY};
	}

	.input-group-text {
		height: 50px;
	}
`;

export const ModalHeader = styled.div`
	.main-menu {
	    padding: 20px 0;
	}

	.main-menu h2 {
	    text-align: center;
	    border-bottom: 1px solid #f7b7b7;
	    margin: 0 20px;
	    padding-bottom: 10px;
	    color: ${defaultColor.PRIMARY};
	}

	.list-menu ul {
		padding: 0;
	}

	.list-menu ul li {
	    list-style: none;
	    padding: 10px 20px;
	    font-weight: 600;
	    color: ${defaultColor.PRIMARY};
	}

	.list-menu ul li:hover {
	    background: #953e461c;
	}
`;

