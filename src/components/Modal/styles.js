import styled from 'styled-components';
import { defaultColor } from '../../utils/defaultColor';

export const ModalWrapper = styled.div`
	background: rgb(149 62 70 / 50%);
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    margin: 0;
    padding: 0;
    z-index: 3;
`;

export const ModalContent = styled.div`
	background: ${defaultColor.SECONDARY};
	position: absolute;
    width: 300px;
    height: 400px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 10px;

    .close-btn {
    	position: absolute;
    	bottom: -40px;
    	left: 50%;
    	font-size: 20px;
	    color: ${defaultColor.SECONDARY};
	    font-weight: bold;
    }
`;

