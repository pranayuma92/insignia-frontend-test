import React from 'react';
import { ModalWrapper, ModalContent } from './styles';

const Modal = ({ children, isOpen, close }) => {
	return (
		<>
			{
				isOpen && (
					<ModalWrapper>
						<ModalContent>
							{children}
							<div className="close-btn" onClick={close}>X</div>
						</ModalContent>
					</ModalWrapper>
				)
			}
		</>
	);
};

export default Modal;

