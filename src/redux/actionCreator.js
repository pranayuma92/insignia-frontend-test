import { request } from '../utils/request';

export const GET_POSTS_LISTS = 'GET_POSTS_LISTS';
export const GET_COMMENTS_LISTS = 'GET_COMMENTS_LISTS';

export const getPosts = () => async (dispatch) => {
	try {
		const { status, data, error } = await request('GET', 'post?limit=5');
		dispatch({
			type: GET_POSTS_LISTS,
			payload: data.data
		});
	} catch (err) {

	}
};

export const getComments = () => async (dispatch) => {
	try {
		const { status, data, error } = await request('GET', 'post/60d21af267d0d8992e610b8d/comment?limit=10');
		dispatch({
			type: GET_COMMENTS_LISTS,
			payload: data.data
		});
	} catch (err) {

	}
}