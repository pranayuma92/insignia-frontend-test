import {
	GET_POSTS_LISTS,
	GET_COMMENTS_LISTS
} from './actionCreator';

const initialState = {
	posts: [],
	comments: []
};

export const appReducer = (state = initialState, action) => {
	const { type } = action;

	switch(type) {
		case GET_POSTS_LISTS:
			return {
				...state,
				posts: action.payload
			}
		case GET_COMMENTS_LISTS:
			return {
				...state,
				comments: action.payload
			}
		default:
			return state;
	}
};
